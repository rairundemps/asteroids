﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenWraper : MonoBehaviour
{
    float colliderRadius;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // OnBecameInvisible called when the renderer is no longer visible by any camera
    private void OnBecameInvisible()
    {
        // get current collider radius for wrapping
        colliderRadius = GetComponent<CircleCollider2D>().radius;

        // check ship position for ship wrapping
        Vector3 position = transform.position;
        if (position.x + colliderRadius < ScreenUtils.ScreenLeft)
        {
            position.x = ScreenUtils.ScreenRight + colliderRadius;
        }
        else if (position.x + colliderRadius > ScreenUtils.ScreenRight)
        {
            position.x = ScreenUtils.ScreenLeft - colliderRadius;
        }
        if (position.y + colliderRadius < ScreenUtils.ScreenBottom)
        {
            position.y = ScreenUtils.ScreenTop + colliderRadius;
        }
        else if (position.y + colliderRadius > ScreenUtils.ScreenTop)
        {
            position.y = ScreenUtils.ScreenBottom - colliderRadius;
        }
        transform.position = position;
    }
}
