﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Bullet insteractions
/// </summary>
public class Bullet : MonoBehaviour
{
    // Life time of bullet
    const float LifeTime = 2;
    Timer spawnTimer;

    // Start is called before the first frame update
    void Start()
    {
        // start timer for bullet lifetime
        spawnTimer = gameObject.AddComponent<Timer>();
        spawnTimer.Duration = LifeTime;
        spawnTimer.Run();
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnTimer.Finished)
        {
            Destroy(gameObject);
        }
    }

    // Apply force to the direction for moving
    public void ApplyForce(Vector2 direction)
    {
        const float Magnitude = 7f;

        GetComponent<Rigidbody2D>().AddForce(direction * Magnitude, ForceMode2D.Impulse);
    }
}
