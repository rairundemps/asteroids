﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// HUD with score for the game
/// </summary>
public class HUD : MonoBehaviour
{
    [SerializeField]
    Text text;
    float elapsedSeconds = 0;
    bool timerIsRunning = true;

    // Start is called before the first frame update
    void Start()
    {
        text.text = "0";
    }

    // Update is called once per frame
    void Update()
    {
        // check if timer is running
        if (timerIsRunning)
        {
            // add time to our timer
            elapsedSeconds += Time.deltaTime;
            text.text = elapsedSeconds.ToString();
        }
    }

    public void StopGameTimer()
    {
        timerIsRunning = false;
    }
}
