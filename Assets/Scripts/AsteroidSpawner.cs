﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spawns asteroids in the game
/// </summary>
public class AsteroidSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject prefabAsteroid;

    // Start is called before the first frame update
    void Start()
    {
        // retrieve circle collider radius
        GameObject asteroid = Instantiate(prefabAsteroid) as GameObject;
        float colliderRadius = asteroid.GetComponent<CircleCollider2D>().radius;
        Destroy(asteroid);

        // create an asteroid moving left
        Vector3 location = new Vector3(ScreenUtils.ScreenRight + colliderRadius,
            0, -Camera.main.transform.position.z);
        Vector3 worldLocation = Camera.main.ScreenToWorldPoint(location);
        GameObject asteroidMoveLeft = Instantiate(prefabAsteroid) as GameObject;
        asteroidMoveLeft.GetComponent<Asteroid>().Initialize(Direction.Left, location);

        // create an asteroid moving down
        location = new Vector3(0, ScreenUtils.ScreenTop + colliderRadius,
            -Camera.main.transform.position.z);
        worldLocation = Camera.main.ScreenToWorldPoint(location);
        GameObject asteroidMoveDown = Instantiate(prefabAsteroid) as GameObject;
        asteroidMoveDown.GetComponent<Asteroid>().Initialize(Direction.Down, location);

        // create an asteroid moving right
        location = new Vector3(ScreenUtils.ScreenLeft - colliderRadius, 0,
            -Camera.main.transform.position.z);
        worldLocation = Camera.main.ScreenToWorldPoint(location);
        GameObject asteroidMoveRight = Instantiate(prefabAsteroid) as GameObject;
        asteroidMoveRight.GetComponent<Asteroid>().Initialize(Direction.Right, location);

        // create an asteroid moving up
        location = new Vector3(0, ScreenUtils.ScreenBottom - colliderRadius,
            -Camera.main.transform.position.z);
        worldLocation = Camera.main.ScreenToWorldPoint(location);
        GameObject asteroidMoveUp = Instantiate(prefabAsteroid) as GameObject;
        asteroidMoveUp.GetComponent<Asteroid>().Initialize(Direction.Up, location);
    }
}
