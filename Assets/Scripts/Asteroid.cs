﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Asteroid behavior
/// </summary>
public class Asteroid : MonoBehaviour
{
    // different asteroids sprites for spawning
    [SerializeField]
    Sprite[] asteroidSprites = new Sprite[3];

    /// <summary>
    /// Initialize asteroid with basic characteristics
    /// </summary>
    /// <param name="direction">Direction of asteroid movement with random 30 degree spread</param>
    /// <param name="position">Position of asteroid</param>
    public void Initialize(Direction direction, Vector3 position)
    {
        // set position
        transform.position = position;

        // calculate random angle based on its direction
        float baseAngle;
        if (direction == Direction.Right)
        {
            baseAngle = 345 * Mathf.Deg2Rad;
        }
        else if (direction == Direction.Up)
        {
            baseAngle = 75 * Mathf.Deg2Rad;
        }
        else if (direction == Direction.Left)
        {
            baseAngle = 165 * Mathf.Deg2Rad;
        }
        else
        {
            baseAngle = 255 * Mathf.Deg2Rad;
        }
        float additionalAngle = Random.Range(0, 30 * Mathf.Deg2Rad);
        float fullAngle = baseAngle + additionalAngle;
        StartMoving(fullAngle);
    }

    /// <summary>
    /// Apply force to asteroid
    /// </summary>
    /// <param name="angle">Angle angle to direct asteroid move</param>
    public void StartMoving(float angle)
    {
        // apply impulse force
        const float MinImpulseForce = 3f;
        const float MaxImpulseForce = 5f;
        float magnitude = Random.Range(MinImpulseForce, MaxImpulseForce);
        Vector2 moveDirection = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
        GetComponent<Rigidbody2D>().AddForce(moveDirection * magnitude, ForceMode2D.Impulse);
    }

    // Start is called before the first frame update
    void Start()
    {
        // get random sprite for asteroid
        GetComponent<SpriteRenderer>().sprite = asteroidSprites[Random.Range(0, 3)];
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject collisionObject = collision.gameObject;
        if (collisionObject.tag == "Bullet")
        {
            AudioManager.Play(AudioClipName.AsteroidHit);
            // check for creating smaller asteroids
            if (transform.localScale.x >= 0.5)
            {
                // calculate new data for two smaller asteroids
                Vector3 newScale = transform.localScale;
                newScale.x *= 0.5f;
                newScale.y *= 0.5f;
                transform.localScale = newScale;
                GetComponent<CircleCollider2D>().radius *= 0.5f;

                // create 2 smaller asteroids
                CreateSmallerAsteroid();
                CreateSmallerAsteroid();
            }

            // Destroy current asteroid and bullet
            Destroy(collisionObject);
            Destroy(gameObject);
        }
    }

    void CreateSmallerAsteroid()
    {
        GameObject smallerAsteroid = Instantiate(gameObject) as GameObject;
        smallerAsteroid.GetComponent<Asteroid>().StartMoving(Random.Range(0, 2 * Mathf.PI));
    }
}
