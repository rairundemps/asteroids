﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Describes the behavior of ship object
/// </summary>
public class Ship : MonoBehaviour
{
    [SerializeField]
    GameObject prefabBullet;
    [SerializeField]
    GameObject hud;

    Rigidbody2D shipRigidbody2D;

    Vector2 thrustDirection = new Vector2(1, 0);
    const float ThrustForce = 3f;
    const int RotateDegreesPerSecond = 180;

    // Start is called before the first frame update
    void Start()
    {
        // get ship rigidbody for use
        shipRigidbody2D = GetComponent<Rigidbody2D>();
    }

    // FixedUpdate is called every fixed time
    private void FixedUpdate()
    {
        if (Input.GetAxis("Thrust") > 0)
        {
            shipRigidbody2D.AddForce(ThrustForce * thrustDirection,
                ForceMode2D.Force);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // check and rotate the ship
        float rotationInput = Input.GetAxis("Rotate");
        if (rotationInput != 0)
        {
            float rotationAmount = RotateDegreesPerSecond * Time.deltaTime;
            if (rotationInput < 0)
            {
                rotationAmount *= -1;
            }
            transform.Rotate(Vector3.forward, rotationAmount);

            // apply new fly direction with force to the ship
            float angle = transform.eulerAngles.z;
            thrustDirection = new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad),
                Mathf.Sin(angle * Mathf.Deg2Rad));
            shipRigidbody2D.velocity = thrustDirection * shipRigidbody2D.velocity.magnitude;
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            GameObject bullet = Instantiate(prefabBullet, transform.position, transform.rotation);
            bullet.GetComponent<Bullet>().ApplyForce(thrustDirection);
            AudioManager.Play(AudioClipName.PlayerShot);
        }
    }

    // Detects the collision
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Asteroid")
        {
            AudioManager.Play(AudioClipName.PlayerDeath);
            hud.GetComponent<HUD>().StopGameTimer();
            Destroy(gameObject);
        }
    }
}
